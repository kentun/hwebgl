#HWebgl
========================================


觉得 webgl 原生太复杂 第三方插件也不简单，只想傻瓜的使用独立的3d功能的，来看看 HWebgl吧，使用就是这么简单。

理想抒发完了，接下来一步一步去实现吧。需要的同学尽情提意见哈


#当前状态 第一个功能优化中....

第一个功能    web全景
--------------------------------------------------------------

使用说明 ：

### 1 先引用插件：
              <script src="../lib/HWebgl.js"></script>

### 2 主体代码(好简单)：
              var textureArray=['myTexture1','myTexture2','myTexture3','myTexture4','myTexture5','myTexture6']
              //参数: 正面图 id,背面图 id ,左侧id, 右侧id, 顶部id, 底部id
              var hwebgl=new HWebgl('webgl');//参数: canvas id
              hwebgl.CreatePano(textureArray,60.0)
              //参数: 贴图数组,视线角度（效果为远近）
              //hwebgl.SetAngle(50.0)//设置远近函数  可用于滚轮 或移动多点 缩放

## 运行环境 
   web站点 

## 演示地址
   http://bciworld.cn/hwebgl/sample/panorama.html

## 文件说明
   1 lib/HWebgl.js         插件源码<br>
   2 lib/HWebgl.min.js     插件压缩文件<br>
   3 lib/hwebgltest01.js   本人开发使用请忽略<br>
   4 lib/hwebgltest02.js   本人开发使用请忽略<br>
   5 sample/               一个案例<br>
   6 res                   全景图片<br>

## 版本：
   2017-4-14  :最基础板上线<br>
   2017-4-14 晚: 功能优化 ：支持移动版,提高纹理精度及抗锯齿<br>
   2017-4-15  1 功能优化: 增加设置远近函数  可用于滚轮 或移动多点 缩放  hwebgl.SetAngle(50.0)
              2 代码优化: 修改了代码结构 更易于拓展.




